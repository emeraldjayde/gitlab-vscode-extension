import * as vscode from 'vscode';
import {
  DUO_CHAT_WEBVIEW_ID,
  DUO_WORKFLOW_WEBVIEW_ID,
  SECURITY_VULNS_WEBVIEW_ID,
} from '../constants';
import { LsWebviewController } from './ls_webview_controller';
import { WebviewInfo } from './webview_info_provider';
import { WebviewThemePublisher } from './theme/types';
import { createThemeHandlerMiddleware } from './theme/create_theme_handler_middleware';
import { createInitialStateMiddleware } from './theme/create_initial_state_middleware';
import { getWebviewContent } from './get_ls_webview_content';
import { applyMiddleware } from './middleware';
import { registerDuoChatHandlers } from './duo_chat/duo_chat_handlers';
import { WebviewManager } from './webview_manager';
import { WebviewMessageRegistry } from './message_handlers';
import { registerDuoChatCommands } from './duo_chat/duo_chat_commands';
import { LSDuoChatWebviewController } from './duo_chat/duo_chat_controller';

// webviews that show in the VS Code panels, sidebar or other custom views (like activity bar)
const PANEL_WEBVIEW_IDS = [DUO_CHAT_WEBVIEW_ID, DUO_WORKFLOW_WEBVIEW_ID];

// webviews in the editor area (i.e. tabs)
const EDITOR_WEBVIEW_IDS = [SECURITY_VULNS_WEBVIEW_ID, DUO_WORKFLOW_WEBVIEW_ID];

// custom settings for webviews in the editor area
const EDITOR_WEBVIEW_SETTINGS: { [webviewId: string]: { viewColumn: vscode.ViewColumn } } = {
  [SECURITY_VULNS_WEBVIEW_ID]: {
    viewColumn: vscode.ViewColumn.Beside,
  },
};

// converts webview kebab-case strings to camelCase for commands
const kebabToCamelCase = (str: string) =>
  str.replace(/-([a-z])/g, (_, letter) => letter.toUpperCase());

const setupEditorWebview = async (
  webviewInfo: WebviewInfo,
  themePublisher: WebviewThemePublisher,
): Promise<vscode.Disposable> => {
  const commandId = `gl.webview.${kebabToCamelCase(webviewInfo.id)}.show`;

  let panel: vscode.WebviewPanel | undefined;
  const disposables: vscode.Disposable[] = [];

  disposables.push(
    vscode.commands.registerCommand(
      `${commandId}`,
      async (initialState?: Record<string, unknown>) => {
        const viewColumn =
          EDITOR_WEBVIEW_SETTINGS?.[webviewInfo.id]?.viewColumn ?? vscode.ViewColumn.One;

        if (panel && !initialState) {
          panel.reveal(viewColumn);
        } else {
          if (panel) {
            panel.dispose();
          }

          panel = vscode.window.createWebviewPanel(
            webviewInfo.id,
            webviewInfo.title,
            {
              viewColumn,
              preserveFocus: true,
            },
            {
              enableScripts: true,
              retainContextWhenHidden: true,
            },
          );
          panel.webview.html = getWebviewContent(new URL(webviewInfo.uris[0]), webviewInfo.title); // FIXME this is not the right way to pick the uri, this should be platform dependent

          const middlewares = [
            createThemeHandlerMiddleware(themePublisher),
            createInitialStateMiddleware(themePublisher, initialState),
          ];

          applyMiddleware(panel, middlewares);

          panel.onDidDispose(() => {
            panel = undefined;
          });
        }
        return panel;
      },
    ),
  );

  return new vscode.Disposable(() => disposables.forEach(x => x.dispose()));
};

const setupPanelWebview = async (
  webviewInfo: WebviewInfo,
  themePublisher: WebviewThemePublisher,
  webviewMessageRegistry: WebviewMessageRegistry,
): Promise<vscode.Disposable> => {
  const viewId = `gl.webview.${webviewInfo.id}`;
  // Temporary mapping while Duo Workflow supports two set of commands for the same webview
  const panelId = webviewInfo.id === DUO_WORKFLOW_WEBVIEW_ID ? `${viewId}-panel` : viewId;
  const commandId = `${kebabToCamelCase(panelId)}.show`;

  const middlewares = [createThemeHandlerMiddleware(themePublisher)];

  const controllerParams = {
    viewId: panelId,
    url: new URL(webviewInfo.uris[0]), // FIXME this is not the right way to pick the uri, this should be platform dependent
    title: webviewInfo.title,
    middlewares,
  };

  const controller =
    webviewInfo.id === DUO_CHAT_WEBVIEW_ID
      ? new LSDuoChatWebviewController(controllerParams)
      : new LsWebviewController(controllerParams);

  const disposables: vscode.Disposable[] = [];

  // FIXME: we should have a mechanism to restore webview state because VS Code prefers to destroy the web pages when they are not visible (the `retainContextWhenHidden` should be false)
  disposables.push(
    vscode.window.registerWebviewViewProvider(viewId, controller, {
      webviewOptions: { retainContextWhenHidden: true },
    }),
  );

  disposables.push(
    vscode.commands.registerCommand(`${commandId}`, async () => {
      await controller.show();
    }),
  );

  if (webviewInfo.id === DUO_CHAT_WEBVIEW_ID) {
    registerDuoChatHandlers(webviewMessageRegistry, controller as LSDuoChatWebviewController);

    disposables.push(
      await registerDuoChatCommands(
        webviewMessageRegistry,
        controller as LSDuoChatWebviewController,
      ),
    );
  }

  return new vscode.Disposable(() => disposables.forEach(x => x.dispose()));
};

export const setupWebviews = async (
  webviewManager: WebviewManager,
  webviewMessageRegistry: WebviewMessageRegistry,
): Promise<vscode.Disposable> => {
  const allInfos = await webviewManager.getWebviewInfos();
  const panelWebviewInfos = allInfos.filter(i => PANEL_WEBVIEW_IDS.includes(i.id));
  const editorWebviewInfos = allInfos.filter(i => EDITOR_WEBVIEW_IDS.includes(i.id));
  const disposables = await Promise.all([
    ...panelWebviewInfos.map(webviewInfo =>
      setupPanelWebview(webviewInfo, webviewManager, webviewMessageRegistry),
    ),
    ...editorWebviewInfos.map(webviewInfo => setupEditorWebview(webviewInfo, webviewManager)),
  ]);

  return new vscode.Disposable(() => disposables.forEach(x => x.dispose()));
};
