import vscode from 'vscode';
import { LsWebviewController, WebviewControllerProps } from '../ls_webview_controller';
import { USER_COMMANDS } from '../../command_names';
import { DUO_CHAT_WEBVIEW_ID } from '../../constants';

const WAIT_TIMEOUT_MS = 10000;

export class LSDuoChatWebviewController extends LsWebviewController {
  #chatReady = false;

  constructor({ viewId, url, title, middlewares }: WebviewControllerProps) {
    super({ viewId, url, title, middlewares });
  }

  async show() {
    if (!this.view) {
      await vscode.commands.executeCommand(`gl.webview.${DUO_CHAT_WEBVIEW_ID}.focus`);

      await this.#waitForChatReady();
      await this.focusChat();
      return;
    }

    if (this.view.visible) {
      await this.focusChat();
    } else {
      this.view.show();
      await this.#waitForChatReady();
      await this.focusChat();
    }
  }

  async hide() {
    if (this.view?.visible) {
      await vscode.commands.executeCommand('workbench.action.closeSidebar');
    }
  }

  async focusChat() {
    if (this.view?.visible) {
      await vscode.commands.executeCommand(USER_COMMANDS.FOCUS_CHAT);
    }
  }

  setChatReady() {
    this.#chatReady = true;
  }

  async #waitForChatReady(): Promise<void> {
    return new Promise((resolve, reject) => {
      const checkInterval = 200;
      const startTime = Date.now();

      const interval = setInterval(() => {
        if (this.#chatReady) {
          clearInterval(interval);
          resolve();
        } else if (Date.now() - startTime >= WAIT_TIMEOUT_MS) {
          clearInterval(interval);
          reject(new Error(`The webview didn't initialize in ${WAIT_TIMEOUT_MS}ms`));
        }
      }, checkInterval);
    });
  }
}
