import * as vscode from 'vscode';
import { CHAT, DUO_DISABLED_FOR_PROJECT } from '@gitlab-org/gitlab-lsp';
import { FeatureStateManager } from '../language_server/feature_state_manager';
import { diffEmitter } from '../utils/diff_emitter';
import { log } from '../log';

export interface ChatState {
  chatAvailable: boolean;
}

const setChatAvailable = (available: boolean) =>
  vscode.commands.executeCommand('setContext', 'gitlab:chatAvailable', available);

const setChatAvailableForProject = (available: boolean) =>
  vscode.commands.executeCommand('setContext', 'gitlab:chatAvailableForProject', available);

export class ChatStateManager implements vscode.Disposable {
  #subscriptions: vscode.Disposable[] = [];

  #featureStateManager!: FeatureStateManager;

  #chatState: ChatState = { chatAvailable: false };

  #eventEmitter = diffEmitter(new vscode.EventEmitter<ChatState>());

  onChange = this.#eventEmitter.event;

  constructor(featureStateManager: FeatureStateManager) {
    this.#featureStateManager = featureStateManager;

    this.#subscriptions.push(
      this.#featureStateManager.onChange(async states => {
        const chatState = states.find(({ featureId }) => featureId === CHAT);
        if (!chatState) {
          log.warn(
            `Feature state manager can't find the Duo Chat state. We will disable chat. Please report this as a bug.`,
          );
          await setChatAvailable(false);
          return;
        }

        const chatAvailable = chatState.engagedChecks.length === 0;
        const duoEnabledForProject = !chatState.engagedChecks.find(({ checkId }) => {
          return checkId === DUO_DISABLED_FOR_PROJECT;
        });

        await setChatAvailable(chatAvailable);
        await setChatAvailableForProject(duoEnabledForProject);
        this.#chatState.chatAvailable = chatAvailable;

        this.#eventEmitter.fire(this.state);
      }),
    );
  }

  get state(): ChatState {
    return this.#chatState;
  }

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
