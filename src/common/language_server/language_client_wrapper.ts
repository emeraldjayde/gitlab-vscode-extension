import path from 'path';
import vscode from 'vscode';
import {
  CODE_SUGGESTIONS_CATEGORY,
  DidChangeDocumentInActiveEditor,
  QUICK_CHAT_CATEGORY,
  QUICK_CHAT_EVENT,
  QUICK_CHAT_OPEN_TRIGGER as LS_QUICK_CHAT_OPEN_TRIGGER,
  TelemetryNotificationType,
  TokenCheckNotificationParams,
  TokenCheckNotificationType,
  TRACKING_EVENTS,
  AiContextEditorRequests,
  GitDiffRequest,
  FeatureStateChangeNotificationType,
  ClientConfig,
} from '@gitlab-org/gitlab-lsp';
import {
  BaseLanguageClient,
  DidChangeConfigurationNotification,
  DidOpenTextDocumentNotification,
} from 'vscode-languageclient';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';
import {
  getDuoCodeSuggestionsConfiguration,
  getExtensionConfiguration,
  getHttpAgentConfiguration,
  getSecurityScannerConfiguration,
  parseDisabledSupportedLanguages,
} from '../utils/extension_configuration';
import { log } from '../log';
import { GitLabTelemetryEnvironment } from '../platform/gitlab_telemetry_environment';
import { QUICK_CHAT_OPEN_TRIGGER } from '../quick_chat/constants';
import { LSGitProvider } from '../git/ls_git_provider';
import { FeatureFlag } from '../feature_flags/constants';
import { getLocalFeatureFlagService } from '../feature_flags/local_feature_flag_service';
import {
  DEFAULT_WEBVIEW_NOTIFICATION_METHOD,
  DEFAULT_WEBVIEW_REQUEST_METHOD,
  handleWebviewNotification,
  handleWebviewRequest,
  WebviewMessageRegistry,
} from '../webview';
import { FeatureStateManager } from './feature_state_manager';

const createNotifyFn =
  <T>(client: BaseLanguageClient, method: string) =>
  (param: T) =>
    client.sendNotification(method, param);

export class LanguageClientWrapper {
  #client: BaseLanguageClient;

  #suggestionsManager: GitLabPlatformManagerForCodeSuggestions;

  #telemetryEnvironment: GitLabTelemetryEnvironment;

  #lsGitProvider: LSGitProvider;

  #subscriptions: vscode.Disposable[] = [];

  #webviewMessageRegistry: WebviewMessageRegistry;

  #featureStateManager: FeatureStateManager;

  constructor(
    client: BaseLanguageClient,
    suggestionsManager: GitLabPlatformManagerForCodeSuggestions,
    telemetryEnvironment: GitLabTelemetryEnvironment,
    lsGitProvider: LSGitProvider,
    webviewMessageRegistry: WebviewMessageRegistry,
    featureStateManager: FeatureStateManager,
  ) {
    this.#client = client;
    this.#suggestionsManager = suggestionsManager;
    this.#telemetryEnvironment = telemetryEnvironment;
    this.#lsGitProvider = lsGitProvider;
    this.#webviewMessageRegistry = webviewMessageRegistry;
    this.#featureStateManager = featureStateManager;
  }

  async initAndStart() {
    this.#client.registerProposedFeatures();
    this.#subscriptions.push();

    this.#client.onNotification(
      TokenCheckNotificationType,
      (response: TokenCheckNotificationParams) => {
        log.warn(
          `Token validation failed in Language Server: (${response.message}). This can happen with OAuth token refresh. If the rest of the extension works, this won't be a problem.`,
        );
      },
    );

    this.#client.onNotification('$/gitlab/openUrl', ({ url }) =>
      vscode.env.openExternal(url).then(result => {
        if (!result) {
          log.warn(`Unable to open URL: ${url}`);
        }

        return result;
      }),
    );

    this.#client.onNotification('$/gitlab/openFile', ({ filePath }) => {
      const joinedPath = path.join(vscode.workspace.rootPath || '', filePath);

      return vscode.commands.executeCommand('vscode.open', vscode.Uri.file(joinedPath));
    });

    this.#client.onRequest(AiContextEditorRequests.GIT_DIFF, (params: GitDiffRequest) => {
      log.debug(`Received git diff request: ${JSON.stringify(params)}`);
      return this.#handleGitDiffRequest(params);
    });
    this.#client.onRequest(
      DEFAULT_WEBVIEW_REQUEST_METHOD,
      handleWebviewRequest(this.#webviewMessageRegistry),
    );
    this.#client.onNotification(
      DEFAULT_WEBVIEW_NOTIFICATION_METHOD,
      handleWebviewNotification(this.#webviewMessageRegistry),
    );

    this.#client.onNotification(FeatureStateChangeNotificationType, async params => {
      this.#featureStateManager.setStates(params);
    });

    this.#webviewMessageRegistry.initNotifier(
      createNotifyFn(this.#client, DEFAULT_WEBVIEW_NOTIFICATION_METHOD),
    );

    await this.#client.start();
    this.#subscriptions.push({ dispose: () => this.#client.stop() });
    await this.syncConfig();
    await this.#sendOpenTabs();
    await this.#sendActiveDocument();
  }

  syncConfig = async () => {
    const platform = await this.#suggestionsManager.getGitLabPlatform();
    if (!platform) {
      log.warn('There is no GitLab account available with access to suggestions');
      // we let the LS know not to use the previous account
      await this.#client.sendNotification(DidChangeConfigurationNotification.type, {
        settings: { token: '' },
      });
      return;
    }
    const extensionConfiguration = getExtensionConfiguration();
    const httpAgentConfiguration = getHttpAgentConfiguration();
    const codeSuggestionsConfiguration = getDuoCodeSuggestionsConfiguration();
    const securityScannerConfiguration = getSecurityScannerConfiguration();
    const isEnabled = (flag: FeatureFlag) => getLocalFeatureFlagService().isEnabled(flag);
    const settings: ClientConfig = {
      baseUrl: platform.account.instanceUrl,
      token: platform.account.token,
      telemetry: {
        trackingUrl: extensionConfiguration.trackingUrl,
        enabled: this.#telemetryEnvironment.isTelemetryEnabled(),
        actions: [{ action: TRACKING_EVENTS.ACCEPTED }],
      },
      featureFlags: {
        [FeatureFlag.CodeSuggestionsClientDirectToGateway]: isEnabled(
          FeatureFlag.CodeSuggestionsClientDirectToGateway,
        ),
        [FeatureFlag.StreamCodeGenerations]: isEnabled(FeatureFlag.StreamCodeGenerations),
        [FeatureFlag.RemoteSecurityScans]: isEnabled(FeatureFlag.RemoteSecurityScans),
        [FeatureFlag.DuoWorkflowBinary]: isEnabled(FeatureFlag.DuoWorkflowBinary),
      },
      openTabsContext: codeSuggestionsConfiguration.openTabsContext,
      suggestionsCache: codeSuggestionsConfiguration.suggestionsCache,
      codeCompletion: {
        additionalLanguages: codeSuggestionsConfiguration.additionalLanguages,
        disabledSupportedLanguages: parseDisabledSupportedLanguages(
          codeSuggestionsConfiguration.enabledSupportedLanguages,
        ),
      },
      logLevel: extensionConfiguration.debug ? 'debug' : 'info',
      projectPath: platform.project?.namespaceWithPath ?? '',
      ignoreCertificateErrors: extensionConfiguration.ignoreCertificateErrors,
      httpAgentOptions: {
        ...httpAgentConfiguration,
      },
      securityScannerOptions: {
        enabled: securityScannerConfiguration.enabled,
      },
      duo: {
        enabledWithoutGitlabProject: extensionConfiguration.duo.enabledWithoutGitLabProject,
        workflow: {
          dockerSocket: extensionConfiguration.duo.workflow.dockerSocket,
          useDocker: extensionConfiguration.duo.workflow.useDocker,
        },
      },
    };

    log.info(`Configuring Language Server - baseUrl: ${platform.account.instanceUrl}`);
    await this.#client.sendNotification(DidChangeConfigurationNotification.type, {
      settings,
    });
  };

  sendSuggestionAcceptedEvent = async (trackingId: string, optionId?: number) =>
    this.#client.sendNotification(TelemetryNotificationType.method, {
      category: CODE_SUGGESTIONS_CATEGORY,
      action: TRACKING_EVENTS.ACCEPTED,
      context: { trackingId, optionId },
    });

  sendQuickChatOpenEvent = async ({ trigger }: { trigger: QUICK_CHAT_OPEN_TRIGGER }) => {
    const openTrigger =
      trigger === QUICK_CHAT_OPEN_TRIGGER.SHORTCUT
        ? LS_QUICK_CHAT_OPEN_TRIGGER.SHORTCUT
        : LS_QUICK_CHAT_OPEN_TRIGGER.BTN_CLICK;

    await this.#client.sendNotification(TelemetryNotificationType.method, {
      category: QUICK_CHAT_CATEGORY,
      action: QUICK_CHAT_EVENT.CHAT_OPEN,
      context: { trigger: openTrigger },
    });
  };

  sendQuickChatMessageEvent = async ({ message }: { message: string }) =>
    this.#client.sendNotification(TelemetryNotificationType.method, {
      category: QUICK_CHAT_CATEGORY,
      action: QUICK_CHAT_EVENT.MESSAGE_SENT,
      context: { message },
    });

  async #sendOpenTabs() {
    try {
      const didOpenEvents = vscode.window.tabGroups.all.flatMap(tabGroup =>
        tabGroup.tabs
          .filter(tab => tab.input instanceof vscode.TabInputText)
          .map(tab => {
            const input = tab.input as vscode.TabInputText;

            return this.#sendDidOpenTextDocumentEvent(input.uri).catch(error => {
              log.warn(
                `Failed to send "textDocument.didOpen" event for "${input.uri.toString()}"`,
                error,
              );
              return null;
            });
          }),
      );

      log.debug(
        `Sending ${didOpenEvents.filter(Boolean).length} existing open text documents to language server`,
      );
      await Promise.all(didOpenEvents);
    } catch (error) {
      log.error('Failed to send existing open tabs to language server: ', error);
    }
  }

  async #sendActiveDocument() {
    if (vscode.window.activeTextEditor) {
      await this.#client.sendNotification(
        DidChangeDocumentInActiveEditor,
        vscode.window.activeTextEditor.document.uri.toString(),
      );
    }
  }

  async #sendDidOpenTextDocumentEvent(uri: vscode.Uri) {
    const textDocument = await vscode.workspace.openTextDocument(uri);

    return this.#client.sendNotification(DidOpenTextDocumentNotification.type, {
      textDocument: {
        uri: textDocument.uri.toString(),
        languageId: textDocument.languageId,
        version: textDocument.version,
        text: textDocument.getText(),
      },
    });
  }

  async #handleGitDiffRequest({
    repositoryUri,
    branch,
  }: {
    repositoryUri: string;
    branch?: string;
  }) {
    const uri = vscode.Uri.parse(repositoryUri);
    log.debug(`Getting git diff for ${repositoryUri} with branch ${branch}`);
    const diff = branch
      ? await this.#lsGitProvider.getDiffWithBranch(uri, branch)
      : await this.#lsGitProvider.getDiffWithHead(uri);
    return diff;
  }

  dispose = () => {
    this.#subscriptions.forEach(s => s.dispose());
  };
}
