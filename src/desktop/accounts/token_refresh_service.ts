import * as vscode from 'vscode';
import { ExponentialBackoffCircuitBreaker } from '@gitlab-org/gitlab-lsp';
import { OAuthAccount } from '../../common/platform/gitlab_account';
import { log } from '../../common/log';
import { TokenExchangeService } from '../gitlab/token_exchange_service';
import { AccountService } from './account_service';

export class TokenRefreshService implements vscode.Disposable {
  #accountService: AccountService;

  #tokenExchangeService: TokenExchangeService;

  #subscriptions: vscode.Disposable[] = [];

  #refreshTimeouts: vscode.Disposable[] = [];

  constructor(accountService: AccountService, tokenExchangeService: TokenExchangeService) {
    this.#accountService = accountService;
    this.#tokenExchangeService = tokenExchangeService;

    this.#subscriptions.push(this.#accountService.onDidChange(() => this.#handleAccountsChange()));

    this.#handleAccountsChange();
  }

  dispose(): void {
    this.#clearAllRefreshes();
    this.#subscriptions.forEach(d => d.dispose());
  }

  #clearAllRefreshes(): void {
    this.#refreshTimeouts.forEach(disposable => disposable.dispose());
    this.#refreshTimeouts = [];
  }

  #handleAccountsChange() {
    this.#clearAllRefreshes();

    const oauthAccounts = this.#accountService
      .getAllAccounts()
      .filter((account): account is OAuthAccount => account.type === 'oauth');

    oauthAccounts.map(account => this.#scheduleTokenRefresh(account));
  }

  #scheduleTokenRefresh(account: OAuthAccount) {
    const msUntilExpiry = Math.max(account.expiresAtTimestampInSeconds * 1000 - Date.now(), 0);
    const circuitBreaker = new ExponentialBackoffCircuitBreaker();

    log.debug(
      `Scheduling token refresh for account ${account.id} in ${msUntilExpiry / 1000} seconds`,
    );

    const timeout = setTimeout(() => this.#refreshToken(account, circuitBreaker), msUntilExpiry);
    this.#refreshTimeouts.push(
      new vscode.Disposable(() => {
        clearTimeout(timeout);
      }),
    );
  }

  async #refreshToken(account: OAuthAccount, circuitBreaker: ExponentialBackoffCircuitBreaker) {
    if (circuitBreaker.isOpen()) {
      log.debug(
        `Token refresh circuit breaker is open for account ${account.id}. We'll attempt another refresh soon.`,
      );
      const disposable = circuitBreaker.onClose(async () => {
        disposable.dispose();
        await this.#refreshToken(account, circuitBreaker);
      });
      return;
    }

    try {
      await this.#tokenExchangeService.refreshIfNeeded(account.id);
    } catch (error) {
      circuitBreaker.error();
      log.error('Failed to refresh token:', error);
      // The next call will check circuit breaker state and handle appropriately
      await this.#refreshToken(account, circuitBreaker);
    }
  }
}
