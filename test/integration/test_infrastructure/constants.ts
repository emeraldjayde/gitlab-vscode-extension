export const GITLAB_URL = 'https://test.gitlab.com';
export const API_URL_PREFIX = `${GITLAB_URL}/api/v4`;
export { REMOTE, DEFAULT_VS_CODE_SETTINGS } from './shared_constants';
