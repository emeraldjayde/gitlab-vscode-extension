import path from 'node:path';
import { createRequire } from 'node:module';
import esbuild from 'esbuild';
import { ENVIRONMENTS } from '../constants.mjs';
import { root, run } from './run_utils.mjs';
import { createBrowserPackageJson } from './packages.mjs';
import { prepareWebviews, generateAssets, writePackageJson, commonJobs } from './common_jobs.mjs';

const browserWebviews = {
  vue3: [],
  vue2: ['gitlab_duo_chat', 'security_finding'],
};

function typecheck(signal) {
  return run('tsc', ['-p', root, '--noEmit'], { cancelSignal: signal });
}

const require = createRequire(import.meta.url);

/** @type {import('esbuild').Plugin} */
const pathImportPlugin = {
  name: 'pathImport',
  setup(build) {
    build.onResolve({ filter: /^path$/ }, resolveObj => {
      console.log(
        `[pathImportPlugin] using path-browserify instead of node:path for ${JSON.stringify(resolveObj)}`,
      );
      // For the browser, resolve to the path-browserify module
      const resolvedPath = require.resolve('path-browserify');
      return {
        path: resolvedPath,
        namespace: 'file',
      };
    });
  },
};

/**
 * Build the extension for browser environment
 * @param {string[]} [args=[]] - Build arguments
 * @param {AbortSignal} [signal] - Optional abort signal to cancel the operation
 * @returns {Promise<void>}
 */
async function buildExtension(args = [], signal) {
  await typecheck(signal);

  /** @type {import('esbuild').BuildOptions} */
  const config = {
    entryPoints: [path.join(root, 'src/browser/browser.js')],
    bundle: true,
    outfile: 'dist-browser/browser.js',
    external: [
      'vscode',
      // For the fs fix, see:
      // https://github.com/tree-sitter/tree-sitter/tree/660481dbf71413eba5a928b0b0ab8da50c1109e0/lib/binding_web#cant-resolve-fs-in-node_modulesweb-tree-sitter
      'fs',
      // `graceful-fs` is a dependency of `enhanced-resolve`, used in the LS. The dependency is not needed, as the LS supplies our own fsClient, so we prevent it from being included in the bundle
      'graceful-fs',
    ],
    format: 'cjs',
    sourcemap: true,
    loader: { '.html': 'text' },
    plugins: [pathImportPlugin],
    ...args.reduce((acc, arg) => {
      if (arg === '--minify') {
        acc.minify = true;
      }
      return acc;
    }, {}),
  };

  if (signal) {
    config.signal = signal;
  }

  await esbuild.build(config);
}

export async function buildBrowser() {
  const packageJson = createBrowserPackageJson();

  await commonJobs(ENVIRONMENTS.BROWSER);

  await Promise.all([
    prepareWebviews(browserWebviews, ENVIRONMENTS.BROWSER),
    writePackageJson(packageJson, ENVIRONMENTS.BROWSER),
    buildExtension(['--minify']),
    generateAssets(packageJson, ENVIRONMENTS.BROWSER),
  ]);
}

// eslint-disable-next-line import/no-default-export
export default {};
